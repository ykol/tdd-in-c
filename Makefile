TARGET_EXEC :=			LedDriver

SRC_DIR :=				./src
INC_DIR :=				./include		
LIB_DIR :=				./lib
OBJ_DIR :=				./obj

HEADERS :=				led_driver.h					\

SRCS :=					led_driver.c					\

OBJS :=					$(addprefix $(OBJ_DIR)/, $(SRCS:.c=.o))
INC_FLAGS :=			-I$(INC_DIR)

TEST_DIR :=				./t
TEST_EXEC :=			$(TEST_DIR)/LedDriverTest	
TEST_OBJ_DIR :=			$(TEST_DIR)/obj

UNITY_DIR :=			$(LIB_DIR)/Unity
UNITY_INC :=			$(UNITY_DIR)/src	
UNITY_FIXTURE_INC :=	$(UNITY_DIR)/extras/fixture/src
#UNITY_FLAGS :=			-DUNITY_INCLUDE_CONFIG_H

UNITY_SRCS :=			unity.c							\
						unity_fixture.c

TEST_SRCS :=			$(UNITY_SRCS)					\
						all_tests.c						\
						led_driver_test.c				\
						led_driver_test_runner.c

TEST_INC_FLAGS :=		-I$(UNITY_INC) -I$(UNITY_FIXTURE_INC)

TEST_OBJS :=			$(addprefix $(TEST_OBJ_DIR)/, $(TEST_SRCS:.c=.o))

CC :=					clang

CC_FLAGS :=				-Wall
CC_FLAGS +=				-Wextra
CC_FLAGS +=				-Werror

test: $(TEST_EXEC)
	clear
	./$(TEST_EXEC)

$(TEST_EXEC): $(TEST_OBJS) $(OBJS)
	$(CC) $(TEST_OBJS) $(OBJS) -o $(TEST_EXEC) 

$(TEST_OBJS): | $(TEST_OBJ_DIR)

$(TEST_OBJ_DIR):
	mkdir -p $(TEST_OBJ_DIR)

$(TEST_OBJ_DIR)/%.o: %.c
	$(CC) -c $< -o $@ $(CC_FLAGS) $(UNITY_FLAGS) $(TEST_INC_FLAGS) $(INC_FLAGS)

$(TARGET_EXEC): $(OBJS)
	$(CC) $(OBJS) -o $(TARGET_EXEC) 

$(OBJS): | $(OBJ_DIR)

$(OBJ_DIR):
	mkdir -p $(OBJ_DIR)

$(OBJ_DIR)/%.o: %.c
	$(CC) -c $< -o $@ $(CC_FLAGS) $(INC_FLAGS)

clean:
	rm -f $(OBJS)		
	rm -f $(TEST_OBJS)

fclean: clean
	rm -f $(TARGET_EXEC)
	rm -f $(TEST_EXEC)
	rm -rf $(OBJ_DIR)
	rm -rf $(TEST_OBJ_DIR)

.PHONY: test clean fclean

vpath %.c		$(SRC_DIR) $(TEST_DIR) $(UNITY_DIR)/src $(UNITY_DIR)/extras/fixture/src	
