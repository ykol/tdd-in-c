#include "unity.h"
#include "unity_fixture.h"
#include <string.h>
#include "led_driver.h"

TEST_GROUP(led_driver);

TEST_SETUP(led_driver)
{
}

TEST_TEAR_DOWN(led_driver)
{
}

TEST(led_driver, leds_off_after_create)
{
	uint16_t	virtual_leds = 0xffff;
	led_driver_create(&virtual_leds);
	TEST_ASSERT_EQUAL_HEX16(0, virtual_leds);
}
